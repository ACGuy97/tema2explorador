package com.barranquero.tema2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class EscribirExterna_Activity extends AppCompatActivity {
    EditText edtOp1;
    EditText edtOp2;
    TextView txvResultado, txvFichero;
    Button btnSuma;
    Memoria miMemoria;
    public static final String NOMBRE_FICHERO = "resultado.txt";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escribir_externa);

        edtOp1 = (EditText)findViewById(R.id.edtOperando1);
        edtOp2 = (EditText)findViewById(R.id.edtOperando2);
        btnSuma = (Button)findViewById(R.id.btnSuma);
        txvResultado = (TextView)findViewById(R.id.txvResultado1);
        txvFichero = (TextView)findViewById(R.id.txvFichero);

        miMemoria = new Memoria(getApplicationContext().getFilesDir().toString());
    }

    public void onClick(View v) {
        String op1 = edtOp1.getText().toString();
        String op2 = edtOp2.getText().toString();
        String texto = "0";
        int resultado;

        resultado = Integer.parseInt(op1) + Integer.parseInt(op2);
        texto = String.valueOf(resultado);

        txvResultado.setText(texto);

        if (miMemoria.escribirExterna(NOMBRE_FICHERO, texto, false, "UTF-8"))
            txvFichero.setText(miMemoria.mostrarPropiedadesExterna(NOMBRE_FICHERO));
        else
            txvFichero.setText("Error de escritura");
    }
}
