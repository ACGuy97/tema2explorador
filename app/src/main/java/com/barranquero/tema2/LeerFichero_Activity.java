package com.barranquero.tema2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LeerFichero_Activity extends AppCompatActivity {
    EditText mEdtNumero, mEdtValor, mEdtDatoInt, mEdtDatoSd;
    TextView mTxvResultado;
    Memoria miMemoria;
    public static final String NUMERO = "numero";
    public static final String VALOR = "valor.txt";
    public static final String DATO = "dato.txt";
    public static final String DATO_SD = "dato_sd.txt";
    public static final String OPERACIONES = "operaciones.txt";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leer_fichero);

        mEdtNumero = (EditText) findViewById(R.id.edtNumero);
        mEdtValor = (EditText) findViewById(R.id.edtValor);
        mEdtDatoInt = (EditText) findViewById(R.id.edtDatoInt);
        mEdtDatoSd = (EditText) findViewById(R.id.edtDatoSd);

        mTxvResultado = (TextView) findViewById(R.id.txvResultadoLeer);

        miMemoria = new Memoria(getApplicationContext());

        iniciar();
    }

    private void iniciar() {
        Resultado miResultado = miMemoria.leerRaw(NUMERO);
        if (miResultado.getCodigo()) {
            mEdtNumero.setText(miResultado.getContenido());
        } else {
            Toast.makeText(this, miResultado.getMensaje(), Toast.LENGTH_SHORT).show();
            mEdtNumero.setText("0");
        }
        miResultado = miMemoria.leerAsset(VALOR);
        if (miResultado.getCodigo()) {
            mEdtValor.setText(miResultado.getContenido());
        } else {
            Toast.makeText(this, miResultado.getMensaje(), Toast.LENGTH_SHORT).show();
            mEdtValor.setText("0");
        }
        if (miMemoria.escribirInterna(DATO, "5", false, "UTF-8")) {
            miResultado = miMemoria.leerInterna(DATO, "UTF-8");
            if (miResultado.getCodigo()) {
                mEdtDatoInt.setText(miResultado.getContenido());
            } else {
                Toast.makeText(this, miResultado.getMensaje(), Toast.LENGTH_SHORT).show();
                mEdtDatoInt.setText("0");
            }
        } else {
            Toast.makeText(this, "Error al escribir en mem. interna", Toast.LENGTH_SHORT).show();
            mEdtDatoInt.setText("0");
        }
        if (miMemoria.disponibleEscritura()) {
            if (miMemoria.escribirExterna(DATO_SD, "7", false, "UTF-8")) {
                miResultado = miMemoria.leerExterna(DATO_SD, "UTF-8");
                if (miResultado.getCodigo()){
                    mEdtDatoSd.setText(miResultado.getContenido());
                } else {
                    Toast.makeText(this, miResultado.getMensaje(), Toast.LENGTH_SHORT).show();
                    mEdtDatoSd.setText("0");
                }
            } else {
                Toast.makeText(this, "Error al escribir en mem. externa", Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Memoria externa no disponible", Toast.LENGTH_SHORT).show();
        }
    }

    public void getOnClick(View view) {
        String op1 = mEdtNumero.getText().toString();
        String op2 = mEdtValor.getText().toString();
        String op3 = mEdtDatoInt.getText().toString();
        String op4 = mEdtDatoSd.getText().toString();
        String texto;
        String mensaje;
        int r;
        try {
            r = Integer.parseInt(op1) + Integer.parseInt(op2) + Integer.parseInt(op3) + Integer.parseInt(op4);
            texto = op1 + "+" + op2 + "+" + op3 + "+" + op4 + "=" + String.valueOf(r);

        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            texto = "0";
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        mTxvResultado.setText(texto);
        if (miMemoria.disponibleEscritura()) {
            if (miMemoria.escribirExterna(OPERACIONES, texto + '\n', true, "UTF-8"))
                mensaje = "Fichero " + OPERACIONES + " escrito correctamente";
            else
                mensaje = "Error al escribir el memoria externa";
        } else {
            mensaje = "Memoria externa no disponible";
        }
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
