package com.barranquero.tema2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

public class Codificacion_Activity extends AppCompatActivity {
    EditText mEdtFicheroLectura, mEdtFicheroEscritura, mEdtContenido;
    Button mBtnLeer, mBtnGuardar;
    RadioButton mRdbUTF8, mRdbUTF16, mRdbISO;
    public static final String UTF8 = "UTF-8";
    public static final String UTF16 = "UTF-16";
    public static final String ISO = "ISO-8859-15";

    Memoria miMemoria;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_codificacion);

        iniciar();
    }

    private void iniciar() {
        mEdtFicheroLectura = (EditText) findViewById(R.id.edtFichero);
        mEdtFicheroEscritura = (EditText) findViewById(R.id.edtNuevoFichero);
        mEdtContenido = (EditText) findViewById(R.id.edtContenido);

        mBtnLeer = (Button) findViewById(R.id.btnLeer);
        mBtnGuardar = (Button) findViewById(R.id.btnGuardar);

        mRdbUTF8 = (RadioButton) findViewById(R.id.rdbUtf8);
        mRdbUTF16 = (RadioButton) findViewById(R.id.rdbUtf16);
        mRdbISO = (RadioButton) findViewById(R.id.rdbIso);

        miMemoria = new Memoria(this.getApplicationContext());
    }

    public void onClickEvent(View view) {
        String codigo = UTF8, nombreFichero;
        Resultado r;

        if (mRdbUTF16.isChecked())
            codigo = UTF16;
        else if (mRdbISO.isChecked())
            codigo = ISO;

        if (view == mBtnLeer) {
            nombreFichero = mEdtFicheroLectura.getText().toString();
            if (nombreFichero.isEmpty())
                Toast.makeText(this, "No hay nombre de fichero", Toast.LENGTH_SHORT).show();
            else {
                r = miMemoria.leerExterna(nombreFichero, codigo);
                if (r.getCodigo())
                    mEdtContenido.setText(r.getContenido());
                else {
                    Toast.makeText(this, r.getMensaje(), Toast.LENGTH_SHORT).show();
                    mEdtContenido.setText("");
                }
            }
        }
        if (view == mBtnGuardar) {
            nombreFichero = mEdtFicheroEscritura.getText().toString();
            if (nombreFichero.isEmpty())
                Toast.makeText(this, "No hay nombre de fichero", Toast.LENGTH_SHORT).show();
            else {
                if (miMemoria.disponibleEscritura())
                    if (miMemoria.escribirExterna(nombreFichero, mEdtContenido.getText().toString(), false, codigo))
                        Toast.makeText(this, "Fichero escrito con éxito", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(this, "Error al escribir en memoria externa", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this, "Memoria externa no disponible", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
