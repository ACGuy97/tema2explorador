package com.barranquero.tema2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MenuPrincipal_Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);
    }

    public void onClick(View view){
        Intent intent;
        switch (view.getId()){
            case R.id.btnEj1:
                intent = new Intent(MenuPrincipal_Activity.this, EscribirInterna_Activity.class);
                startActivity(intent);
                break;
            case R.id.btnEj2:
                intent = new Intent(MenuPrincipal_Activity.this, EscribirExterna_Activity.class);
                startActivity(intent);
                break;
            case R.id.btnEj3:
                intent = new Intent(MenuPrincipal_Activity.this, LeerFichero_Activity.class);
                startActivity(intent);
                break;
            case R.id.btnEj4:
                intent = new Intent(MenuPrincipal_Activity.this, Codificacion_Activity.class);
                startActivity(intent);
                break;
            case R.id.btnEj5:
                intent = new Intent(MenuPrincipal_Activity.this, Explorador_Activity.class);
                startActivity(intent);
                break;
        }
    }
}
