package com.barranquero.tema2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Explorador_Activity extends AppCompatActivity {
    private static final int ABRIRFICHERO_RESULT_CODE = 1;
    private Button botonAbrir;
    private TextView txtInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explorador);

        botonAbrir = (Button) findViewById(R.id.btnAbrir);
        txtInfo = (TextView) findViewById(R.id.txvInfo);
    }

    public void onClickEvent2(View v){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(intent, ABRIRFICHERO_RESULT_CODE);
        else
//informar que no hay ninguna aplicación para manejar ficheros
            Toast.makeText(this, "No hay aplicación para manejar ficheros", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (requestCode == ABRIRFICHERO_RESULT_CODE)
            if (resultCode == RESULT_OK) {
// Mostramos en la etiqueta la ruta del archivo seleccionado
                String ruta = data.getData().getPath();
                txtInfo.setText(ruta);
            }
    }
}
